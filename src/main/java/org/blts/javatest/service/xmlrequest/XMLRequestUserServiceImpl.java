package org.blts.javatest.service.xmlrequest;

import org.blts.javatest.dao.xmlrequest.XMLRequestUserDAO;
import org.blts.javatest.data.entities.XMLRequestUser;
import org.blts.javatest.service.exception.XMLServiceRequestException;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ntsako on 2017/11/08.
 */
@Service("xmlRequestUserService")
public class XMLRequestUserServiceImpl implements XMLRequestUserService {

    @Autowired
    @Qualifier("xmlRequestUserDao")
    private XMLRequestUserDAO xmlRequestUserDAO;

    @Transactional
    @Override
    public XMLRequestUser findUserByUsername(String username) throws XMLServiceRequestException {
        XMLRequestUser user = xmlRequestUserDAO.findByUsername(username);
        try {
            user = xmlRequestUserDAO.findByUsername(username);
        } catch (HibernateException exc) {
            throw new XMLServiceRequestException("DB Error", exc);
        }
        if (user == null)
            throw new XMLServiceRequestException("User not found");
        return user;
    }
}
