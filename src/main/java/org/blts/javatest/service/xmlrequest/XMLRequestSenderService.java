package org.blts.javatest.service.xmlrequest;

import org.blts.javatest.service.exception.XMLServiceRequestException;

/**
 * Created by ntsako on 2017/11/08.
 */
public interface XMLRequestSenderService {

    String sendXmlRequest() throws XMLServiceRequestException;
}
