package org.blts.javatest.service.xmlrequest;

import org.blts.javatest.data.entities.XMLRequestUser;
import org.blts.javatest.service.exception.XMLServiceRequestException;

/**
 * Created by ntsako on 2017/11/08.
 */
public interface XMLRequestUserService {

    XMLRequestUser findUserByUsername(String username) throws XMLServiceRequestException;

}
