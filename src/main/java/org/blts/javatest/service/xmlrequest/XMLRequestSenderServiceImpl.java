package org.blts.javatest.service.xmlrequest;

import lombok.extern.slf4j.Slf4j;
import org.blts.javatest.xml.XMLRequestUtils;
import org.blts.javatest.xml.XMLRequestWriter;
import org.blts.javatest.service.exception.XMLServiceRequestException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

/**
 * Created by ntsako on 2017/11/08.
 */
@Slf4j
@Service("xmlRequestSenderService")
public class XMLRequestSenderServiceImpl implements XMLRequestSenderService {

    @Autowired
    @Qualifier("xmlRequestWriter")
    private XMLRequestWriter xmlRequestWriter;

    @Override
    public String sendXmlRequest() throws XMLServiceRequestException {
        try {
          return   xmlRequestWriter.writeXMLRequest(XMLRequestUtils.createXMLDoc("xmlrequest.xml"));
        } catch (ParserConfigurationException | SAXException  | IOException | IllegalStateException ex) {
            throw new XMLServiceRequestException(ex.getMessage(), ex);
        }
    }


}
