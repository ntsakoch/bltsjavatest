package org.blts.javatest.service.exception;

/**
 * Created by ntsako on 2017/11/08.
 */
public class XMLServiceRequestException extends Exception {

    public XMLServiceRequestException(String message) {
        super(message);
    }

    public XMLServiceRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
