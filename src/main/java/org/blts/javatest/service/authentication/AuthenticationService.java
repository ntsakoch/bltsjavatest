package org.blts.javatest.service.authentication;

import org.blts.javatest.service.exception.XMLServiceRequestException;
/**
 * Created by ntsako on 2017/11/08.
 */
public interface AuthenticationService {

    String findLoggedInUsername() throws XMLServiceRequestException;

    boolean authenticate(String username, String password) throws XMLServiceRequestException;
}
