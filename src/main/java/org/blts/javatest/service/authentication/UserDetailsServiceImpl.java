package org.blts.javatest.service.authentication;


import org.blts.javatest.dao.xmlrequest.XMLRequestUserDAO;
import org.blts.javatest.data.entities.XMLRequestUser;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ntsako on 2017/11/08.
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    @Qualifier("xmlRequestUserDao")
    private XMLRequestUserDAO xmlRequestUserDAO;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        XMLRequestUser user;
        try {
            user = xmlRequestUserDAO.findByUsername(username);
        } catch (HibernateException e) {
            throw new UsernameNotFoundException("User Not Found.",e);
        }
        if (user == null) {
            throw new UsernameNotFoundException("User Not Found.");
        }
        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole()));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }
}
