package org.blts.javatest.xml;

import org.blts.javatest.service.exception.XMLServiceRequestException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by ntsako on 2017/11/09.
 */
public class XMLRequestUtils {

    private XMLRequestUtils(){}

    public static String parseXMLDocument(Document inDoc) throws XMLServiceRequestException
    {
        StringWriter writer = new StringWriter();
        try {
            DOMSource domSource = new DOMSource(inDoc);
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
        } catch (TransformerException e) {
            throw new XMLServiceRequestException(e.getMessage(), e);
        }
        return writer.toString();
    }

    public static Document createXMLDoc(String xmlFilePath) throws IOException, ParserConfigurationException, SAXException {
        Resource resource = new ClassPathResource(xmlFilePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(resource.getInputStream());
        doc.getDocumentElement().normalize();
        return doc;
    }
}
