package org.blts.javatest.xml;

import lombok.extern.slf4j.Slf4j;
import org.blts.javatest.service.exception.XMLServiceRequestException;
import org.blts.javatest.socket.ServerSocketConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.Socket;

/**
 * Created by ntsako on 2017/11/09.
 */
@Slf4j
@Component("xmlRequestWriter")

public class XMLRequestWriter {

    @Autowired
    @Qualifier("serverSocketConnection")
    private ServerSocketConnection sockectConnection;

    public String writeXMLRequest(Document xmlDoc) throws IOException, IllegalStateException, XMLServiceRequestException {

        if (!sockectConnection.isInitialized())
            throw new IllegalStateException("ServerSocket Not Initialized or Closed.");

        Socket socket = sockectConnection.getSocketConnection();
        if (socket.isClosed())
            throw new IllegalStateException("ServerSocket is closed.");

        DataOutputStream dataOutputStream = null;
        try {
            log.info("About to send request to: {}:{}", socket.getInetAddress().getHostAddress(), socket.getPort());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            byte[] dataToSend = XMLRequestUtils.parseXMLDocument(xmlDoc).getBytes();
            dataOutputStream.writeInt(dataToSend.length);
            dataOutputStream.write(dataToSend);

            log.info("Request sent...reading the response...");
            StringBuilder response= new StringBuilder();
            InputStream is = socket.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String incomingMsg;

            while ((incomingMsg= br.readLine()) != null) {
                incomingMsg += "\n";
                response.append(incomingMsg);
            }

            log.info("Response received from the server : \n{}", response.toString());
            return response.toString();
        } catch (Exception exc) {
            log.error("An error occurred on send: ", exc);
            throw new XMLServiceRequestException(exc.getMessage(), exc);
        } finally {
            if(dataOutputStream!=null)
            dataOutputStream.flush();
        }
    }



}