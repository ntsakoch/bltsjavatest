package org.blts.javatest.xml;

import lombok.extern.slf4j.Slf4j;
import org.blts.javatest.XMLRequestApp;
import org.blts.javatest.service.exception.XMLServiceRequestException;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by ntsako on 2017/11/09.
 */
@Slf4j
@Component("xmlRequestReader")
public class XMLRequestReader implements Runnable {

    private ServerSocket serverSocket = null;

    @PostConstruct
    public void init() {
        try {
            log.info("Opening server socket at port: {}", XMLRequestApp.getServerPort());
            serverSocket = new ServerSocket(XMLRequestApp.getServerPort());
            new Thread(this).start();
            log.info("Server socket opened...now listening for incoming request");
        } catch (IOException e) {
            log.error("An error occurred initializing socketConnection: ", e);
        }
    }

    @Override
    public void run() {
        while (true) {
            Socket socket = null;
            OutputStream outputStream = null;
            InputStream inputStream = null;
            try {
                log.info("Waiting for xml  request");

                socket = serverSocket.accept();
                inputStream = socket.getInputStream();
                String toWriteBack = receive(inputStream);

                log.info("Received xml request:\n{}",toWriteBack);

                outputStream = socket.getOutputStream();
                outputStream.write(toWriteBack.getBytes());


            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (inputStream != null)
                       inputStream.close();
                    if (outputStream != null) {
                        outputStream.flush();
                     //outputStream.close();
                   }
                    if (socket != null) {
                       // socket.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }
    }

    private  String receive(InputStream channel) throws ParserConfigurationException, TransformerConfigurationException, IOException, SAXException, XMLServiceRequestException {

        DocumentBuilderFactory docBuilderFact = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFact.newDocumentBuilder();
        Document incomingXMLDocument;

        XMLInputStream incomingXML = new XMLInputStream(channel);

        incomingXML.recive();
        incomingXMLDocument = docBuilder.parse(incomingXML);
        return XMLRequestUtils.parseXMLDocument(incomingXMLDocument);
    }


class XMLInputStream extends ByteArrayInputStream {

        private DataInputStream inchannel;

        public XMLInputStream(InputStream inchannel) {
            super(new byte[2]);
            this.inchannel = new DataInputStream(inchannel);
        }

        public void recive() throws IOException {
            int i = inchannel.readInt();
            byte[] data = new byte[i];
            inchannel.read(data, 0, i);
            this.buf = data;
            this.count = i;
            this.mark = 0;
            this.pos = 0;
        }

    }


}
