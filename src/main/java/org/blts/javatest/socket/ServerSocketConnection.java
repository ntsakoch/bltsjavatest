package org.blts.javatest.socket;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.blts.javatest.XMLRequestApp;
import org.blts.javatest.xml.XMLRequestReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by ntsako on 2017/11/09.
 */

@Slf4j
@Component("serverSocketConnection")
@Scope(value = "singleton")
@PropertySource({"classpath:xmlrequest.properties"})
public class ServerSocketConnection {

    @Getter
    private boolean initialized = false;

    @Getter
    private Socket socketConnection;

    @Value("${xmlrequest.address}")
    private String serverAddress;

    @Value("${xmlrequest.port}")
    private Integer serverPort;

@Autowired
private XMLRequestReader xmlRequestReader;

    @PostConstruct
    public void init() {
        try {
            log.info("About to open socket connection at address: {}:{}", XMLRequestApp.getServerAddress()/*serverAddress*/,XMLRequestApp.getServerPort()/*serverPort*/);
            socketConnection = new Socket(XMLRequestApp.getServerAddress() /*serverAddress*/, XMLRequestApp.getServerPort()/*serverPort*/);
            initialized =true;
            log.info("Socket Connection successfully established at {}:{} !", socketConnection.getInetAddress().getHostAddress(), socketConnection.getPort());
        } catch (IOException e) {
            log.error("An error occurred initializing socketConnection: ", e);
        }
    }
}
