package org.blts.javatest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Created by ntsako on 2017/11/08.
 */
@SpringBootApplication
public class XMLRequestApp extends SpringBootServletInitializer {

    private static String serverAddress = "localhost";
    private static int port = 9090;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(XMLRequestApp.class);
    }

    public static void main(String[] args) {
        if (args.length > 0) {
            serverAddress = args[0];
            try {
                port = Integer.parseInt(args[1]);
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
        SpringApplication.run(XMLRequestApp.class, args);
    }

    public static String getServerAddress() {
        return serverAddress;
    }

    public static int getServerPort() {
        return port;
    }
}