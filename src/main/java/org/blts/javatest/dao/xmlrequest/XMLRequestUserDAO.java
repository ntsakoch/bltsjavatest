package org.blts.javatest.dao.xmlrequest;

import org.blts.javatest.data.entities.XMLRequestUser;
import org.hibernate.HibernateException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
/**
 * Created by ntsako on 2017/11/08.
 */
@Repository("xmlRequestUserDao")
public interface XMLRequestUserDAO extends CrudRepository<XMLRequestUser, Integer> {

    XMLRequestUser findByUsername(String username) throws HibernateException;

}
