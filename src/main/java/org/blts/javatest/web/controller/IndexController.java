package org.blts.javatest.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.blts.javatest.data.entities.XMLRequestUser;
import org.blts.javatest.service.authentication.AuthenticationService;
import org.blts.javatest.service.exception.XMLServiceRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map;

/**
 * Created by ntsako on 2017/11/08.
 */
@Slf4j
@Controller("indexController")
public class IndexController {

    @Autowired
    @Qualifier("authenticationService")
    private AuthenticationService authenticationService;

    @RequestMapping("/")
    public String getIndexPage(Model model) {
        log.info("on index");
        return "index";
    }

    @RequestMapping(value = "/xmlrequest/user/login",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public void userLogin(@RequestBody XMLRequestUser user, HttpServletRequest request, HttpServletResponse response, HttpSession session) {

        log.info("on userLogin: {}", user);
        try {
            boolean authenticated = false;
            if (session.getAttribute("authenticated") != null) {
                authenticated = Boolean.parseBoolean(String.valueOf(session.getAttribute("authenticated")));
            }
            if (!authenticated) {
                try {
                    log.info("Authentication...");
                    authenticated = authenticationService.authenticate(user.getUsername(), user.getPassword());
                } catch (Exception exc) {
                    log.error("Auth error: ", exc);
                }
            }

            if (authenticated) {
                log.info("user authenticated successfully!");
                session.setAttribute("authenticated", Boolean.TRUE);
                response.sendRedirect(request.getContextPath() + "/xmlrequest/send-request");
            } else {
                log.info("Auth failed!");
                session.setAttribute("authenticated", Boolean.FALSE);
                response.sendRedirect(request.getContextPath() + "/xmlrequest/login-error");
            }

        } catch (IOException exc) {
            log.error("An error occurred on userLogin", exc);
        }
    }
}
