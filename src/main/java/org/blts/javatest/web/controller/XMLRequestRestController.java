package org.blts.javatest.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.blts.javatest.data.entities.XMLRequestUser;
import org.blts.javatest.service.authentication.AuthenticationService;
import org.blts.javatest.service.exception.XMLServiceRequestException;
import org.blts.javatest.service.xmlrequest.XMLRequestSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.AbstractMap;
import java.util.Map;


/**
 * Created by ntsako on 2017/11/08.
 */
@Slf4j
@RestController("xmlRequestUserController")
@RequestMapping("/xmlrequest")
public class XMLRequestRestController {

    @Autowired
    @Qualifier("authenticationService")
    private AuthenticationService authenticationService;

    @Autowired
    @Qualifier("xmlRequestSenderService")
    private XMLRequestSenderService xmlRequestSenderService;


    @ResponseBody
    @RequestMapping(value = "/send-request",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map.Entry<String,String> sendRequest() {
        try {
             String response = xmlRequestSenderService.sendXmlRequest();
                log.info("Request back==>{}",response);
                return new AbstractMap.SimpleEntry<>("response", response);
        } catch (XMLServiceRequestException exc) {
            log.error("An error occurred on userLogin", exc);
            return new AbstractMap.SimpleEntry<>("response", "error: "+exc.getMessage());
        }

    }

    @ResponseBody
    @RequestMapping(value = "/login-error",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map.Entry<String,String> loginError() {
            return new AbstractMap.SimpleEntry<>("response", "user not authenticated");
    }
}
