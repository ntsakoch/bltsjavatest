<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head lang="en">
    <title>Java Test</title>
</head>
<body>
<h1>Login Page</h1>


<form id="login-form">
    <table align="center" width="50%">
        <tr>
            <td>Username: </td>
            <td><input type="text" id="username"></td>
        </tr>
        <tr>
            <td>Password: </td>
            <td><input type="password" id="password"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Login"></td>
        </tr>

    </table>

<p align="center">
Click login to display results...
</p>

</form>
</body>
<script type="text/javascript"  src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript">

jQuery(document).ready(function($) {
	$("#login-form").submit(function(event) {
		event.preventDefault(); // Prevent the form from submitting via the browser.
		login();
	});
});

function display(data)
{
   $("p").text(data);
}

function login() {
        var user = {};
        user["username"] = $("#username").val();
        user["password"]=$("#password").val();

	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : "/xmlrequest/user/login",
		data : JSON.stringify(user),
		dataType : 'json',
		timeout : 100000,
		success : function(data) {
			console.log("SUCCESS: ", data);
			display(JSON.stringify(data));
		},
		error : function(e) {
			console.log("ERROR: ", e);
			display(e);
		},
		done : function(e) {
			console.log("DONE");
			display("Done");
		}
	});
}

</script>
</html>